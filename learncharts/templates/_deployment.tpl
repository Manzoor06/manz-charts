{{- define "helm-base-chart.deployment" -}}
{{- range $service, $serviceValues := .Values.application.services }}
  {{- if $serviceValues.deployment}}
    {{- if $serviceValues.deployment.enabled}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{$service}}
  labels:
    {{- if $serviceValues.labels }}
    {{- range $labelKey, $labelValue := $serviceValues.labels }}
    {{$labelKey}}: {{$labelValue}}
    {{- end }}
    {{- end }}
spec:
  replicas: {{ $serviceValues.deployment.replicaCount | default ($.Values.application.replicaCount)}}
  selector:
    matchLabels:
      {{- range $selectorKey,$selectorValue := $serviceValues.deployment.selectorLabels }}
      {{$selectorKey}}: {{$selectorValue}}
      {{- end }}
  template:
    metadata:
      labels:
        app: {{$service}}
        {{- if $serviceValues.labels }}
        {{- range $labelKey,$labelValue := $serviceValues.labels }}
        {{$labelKey}}: {{$labelValue}}
        {{- end }}
        {{- end }}
    spec:
      containers:
      - name: {{ $serviceValues.deployment.overridename | default ($service) }}
        image: {{ $serviceValues.deployment.image.name }}:{{ $serviceValues.deployment.image.tag }}
        ports:
          - containerPort: {{ $serviceValues.deployment.ports.containerPort }}
        {{- if and $serviceValues.deployment.volumeMounts }}
        {{- $volumeMount := $.Files.Get $serviceValues.deployment.volumeMounts | fromYaml }}
        {{- toYaml $volumeMount | nindent 8 }}
        {{- end }}
        resources:
          requests:
            cpu: "150m"
            memory: "64Mi"
          limits:
            {{- if eq $serviceValues.deployment.resources.limits.cpu "M" }}
            cpu: "300m"            
            {{- else if eq $serviceValues.deployment.resources.limits.cpu "L" }}
            cpu: "500m"
            {{- else }}
            cpu: :250m
            {{- end }}
            {{- if eq $serviceValues.deployment.resources.limits.memory "M" }}
            cpu: "200Mi"            
            {{- else if eq $serviceValues.deployment.resources.limits.memory "L" }}
            cpu: "300Mi"
            {{- else }}
            memory: "100Mi"
            {{- end }}
      {{- if and $serviceValues.deployment.volumes }}
      {{- $volumes := $.Files.Get $serviceValues.deployment.volumes | fromYaml }}
      {{- toYaml $volumes | nindent 6 }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}
{{- end }}