{{- define "helm-base-chart.configmap" -}}
{{- if .Values.application.configMaps }}
  {{- range $configMapName, $valueFile := .Values.application.configMaps }}
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ $configMapName }}
  annotations:
    meta.helm.sh/release-name: {{ $.Release.Name }}
    helm.sh/chart: {{ $.Values.application.release }}-{{ $.Chart.Version }}
  labels:
    app.kubernetes.io/version: {{ $.Chart.Version }}
data:
  {{- $configMapValue := $.Files.Get $valueFile | fromYaml }}
  {{- toYaml $configMapValue | nindent 2 }}
  {{- end }}
{{- end }}
{{- end }}
