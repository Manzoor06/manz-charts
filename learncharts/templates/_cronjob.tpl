{{- define "helm-base-chart.cronjob" -}}
{{- range $service, $serviceValues := .Values.application.services }}
  {{- if $serviceValues.cronjob }}
apiVersion: batch/v1
kind: CronJob
metadata:
  name: {{ $serviceValues.cronjob.overridename | default ($service) }}
  labels:
    app: {{ $serviceValues.cronjob.overridename | default ($service) }}
    version: {{ $serviceValues.cronjob.version | default ($.Chart.Version) }}
    {{- if $serviceValues.labels }}
    {{- range $labelKey, $labelValue := $serviceValues.labels }}
    {{$labelKey}}: {{$labelValue}}
    {{- end }}
    {{- end }}
spec:
  schedule: {{ $serviceValues.cronjob.schedule | default ("1 1 1 1 1") | quote }}
  jobTemplate:
    metadata:
      labels:
        {{- if $serviceValues.labels }}
        {{- range $labelKey, $labelValue := $serviceValues.labels }}
        {{$labelKey}}: {{$labelValue}}
        {{- end }}
        {{- end }}
    spec:
      template:
        spec:
          containers:
          - name: {{ $serviceValues.cronjob.overridename | default ($service) }}
            image: {{ $serviceValues.cronjob.image.name }}:{{ $serviceValues.cronjob.image.tag }}
            imagePullPolicy: {{ $serviceValues.cronjob.image.imagePullPolicy }}
            command:
            - /bin/sh
            - -c
            - date; echo Hello from the k8s cluster
          restartPolicy: {{ $serviceValues.cronjob.restartPolicy }}
  {{- end }}
{{- end }}
{{- end }}
