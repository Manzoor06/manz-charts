{{- define "helm-base-chart.hpa" -}}
{{- range $service, $serviceValues := .Values.application.services }}
  {{- if $serviceValues.deployment }}
  {{- if $serviceValues.deployment.enabled }}
  {{- if $serviceValues.hpa }}
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
metadata:
  name: {{$serviceValues.deployment.overridename | default ($service)}}
# we can have annotations as well
  annotations:
    meta.helm.sh/release-name: {{ $.Release.Name }}
    helm.sh/chart: {{ $.Release.Namespace }}-{{ $.Chart.Version }}
  labels:
  {{- if $serviceValues.labels }}
  {{- range $labelkey,$labelvalue := $serviceValues.labels}}
    {{$labelkey}} : {{$labelvalue}}
  {{- end }}
  {{- end }}
spec:
  scaleTargetRef:
    kind: Deployment
    name: {{$serviceValues.deployment.overridename | default ($service)}}
    apiVersion: apps/v1
  minReplicas: {{ $serviceValues.hpa.minReplicas }}
  maxReplicas: {{ $serviceValues.hpa.maxReplicas }}
  metrics:
    - type: Resource
      resource:
        name: cpu
        target:
          type: Utilization
          averageUtilization: {{ $serviceValues.hpa.maxCpu | default (80)}}
---
  {{- else }}
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
metadata:
  name: {{$serviceValues.deployment.overridename | default ($service)}}
# we can have annotations as well
  annotations:
    meta.helm.sh/release-name: {{ $.Release.Name }}
    helm.sh/chart: {{ $.Release.Namespace }}-{{ $.Chart.Version }}
  labels:
  {{- if $serviceValues.labels }}
  {{- range $labelkey,$labelvalue := $serviceValues.labels}}
    {{$labelkey}} : {{$labelvalue}}
  {{- end }}
  {{- end }}
spec:
  scaleTargetRef:
    kind: Deployment
    name: {{$serviceValues.deployment.overridename | default ($service)}}
    apiVersion: apps/v1
  minReplicas: 1
  maxReplicas: 3
  metrics:
    - type: Resource
      resource:
        name: cpu
        target:
          type: Utilization
          averageUtilization: 80
---
  {{- end }}
  {{- end }}
  {{- end }}
{{- end }}
{{- end }}